import {Telegraf} from 'telegraf';

function getGameUrl(gameShortName) {
    switch (gameShortName) {
        case 'knight':
            return 'https://user890104.gitlab.io/knight-game/';
        default:
            return null;
    }
}

const bot = new Telegraf(process.env.BOT_TOKEN)
    .catch((err, _ctx) => {
        console.error(err);
    });
bot.start(async ctx => ctx.replyWithGame('knight'));
bot.on('inline_query', async ctx => ctx.answerInlineQuery());
bot.gameQuery(async ctx => ctx.answerGameQuery(getGameUrl(ctx.callbackQuery.game_short_name)));

await bot.launch();

// Enable graceful stop
process.once('SIGINT', () => bot.stop('SIGINT'));
process.once('SIGTERM', () => bot.stop('SIGTERM'));
