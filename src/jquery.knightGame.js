/*

    Copyright 2013 Vencislav "Slackware" Atanasov


    This file is part of knightGame.

    knightGame is free software: you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 2 of the
    License, or (at your option) any later version.

    knightGame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with knightGame.  If not, see <http://www.gnu.org/licenses/>.

*/

(function($) {
    function zeroPad(num, maxLength = 2) {
        return num.toString().padStart(maxLength, '0');
    }

    function runAfterRepaint(fn) {
        requestAnimationFrame(function () {
            requestAnimationFrame(fn);
        });
    }

    $.fn.knightGame = function(opts) {
        function updateTimer() {
            if (!game.startDateTime) {
                return;
            }

            const deltaMs = Date.now() - game.startDateTime.getTime();
            const hours = Math.floor(deltaMs / 3_600_000);
            const minutes = Math.floor((deltaMs - hours * 3_600_000) / 60_000);
            const seconds = Math.floor((deltaMs - hours * 3_600_000 - minutes * 60_000) / 1_000);
            const milliseconds = deltaMs % 1_000;
            timer
                .text(`${zeroPad(hours)}:${zeroPad(minutes)}:${zeroPad(seconds)}.${zeroPad(milliseconds, 3)}`);
            requestAnimationFrame(updateTimer);
        }

        const obj = this;

        if (!obj.length || obj.hasClass('knightGame')) {
            return obj;
        }
        
        opts = $.extend({
            width : 10,
            height : 10,
            tileWidth: 30,
            tileHeight: 30,
        }, opts);

        const game = {
            width: opts.width * opts.tileWidth,
            height: opts.height * opts.tileHeight,
            tileCount: opts.width * opts.height,
            openTiles: 0,
            startDateTime: null,
        };

        obj
            .width(game.width)
            .addClass('knightGame');
        
        for (let y = 0; y < opts.height; y++) {
            const row = $('<div />', {
                class: 'row',
            })
                .height(opts.tileHeight);

            for (let x = 0; x < opts.width; x++) {
                row
                    .append(
                        $('<div />', {
                            class: 'tile available',
                        })
                            .width(opts.tileWidth)
                            .height(opts.tileHeight)
                            .css('line-height', opts.tileHeight + 'px')
                            .html('&nbsp;')
                    );
            }
            
            obj
                .append(row);
        }

        const timer = $('<p />', {
            class: 'timer',
        })
            .text('00:00:00.000');

        obj
            .append(timer);

        const offsets = [
            [1, 2],
            [2, 1],
            [1, -2],
            [2, -1],
            [-1, -2],
            [-2, -1],
            [-1, 2],
            [-2, 1],
        ];

        $('.tile', obj)
            .bind('click', function() {
                const $this = $(this);

                if (!$this.hasClass('available')) {
                    return;
                }

                if (!game.startDateTime) {
                    game.startDateTime = new Date();
                    updateTimer();
                }
                
                $this
                    .addClass('selected')
                    .text(String(++game.openTiles));
                
                $('.tile.available', obj)
                    .removeClass('available');
                
                if (game.openTiles === game.tileCount) {
                    game.startDateTime = null;
                    runAfterRepaint(function() {
                        alert('Поздравления! Вие се справихте отлично!');
                    });
                    return;
                }

                const offset = {
                    x: $this.index(),
                    y: $this.parent().index(),
                };

                let nextOffsets = 0;

                $.each(offsets, function(k, v) {
                    const nextOffset = {
                        x: offset.x + v[0],
                        y: offset.y + v[1],
                    };

                    if (nextOffset.x < 0 || nextOffset.y < 0) {
                        return true;
                    }

                    const tile = $('.row:eq(' + nextOffset.y + ') .tile:eq(' + nextOffset.x + ')');

                    if (!tile.length) {
                        return true;
                    }
                    
                    if (tile.hasClass('selected')) {
                        return true;
                    }
                    
                    tile
                        .addClass('available');
                    
                    nextOffsets++;
                });
                
                if (!nextOffsets) {
                    game.startDateTime = null;
                    const percentCompleted = Math.round(game.openTiles / game.tileCount * 100);
                    runAfterRepaint(function() {
                        alert(`Край на играта - няма повече възможни ходове. Резултат: ${percentCompleted}%`);
                    });
                }
            });
        
        return this;
    };
})(jQuery);
